from django.shortcuts import render


from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from common.json import ModelEncoder
from .models import Hats, LocationVO


# Create your views here.
class LocationVODetailEncoder(ModelEncoder): 
    model = LocationVO
    properties = ["closet_name", "section_number", "shelf_number", "import_href", "id"]

class HatsListEncoder(ModelEncoder): 
    model = Hats
    properties = [
        "id", 
        "fabric", 
        "style_name", 
        "color", 
        "url", 
        ]

class HatsDetailEncoder(ModelEncoder): 
    model = Hats
    properties = [
        "fabric", 
        "style_name", 
        "color", 
        "url", 
        "location", 
        "id",
    ]
    encoders = {
        "location": LocationVODetailEncoder()
    }

@require_http_methods(["GET", "POST"])
def api_list_hats(request): 
    if request.method == "GET": 
        hats = Hats.objects.all()
        return JsonResponse (
            {"hats": hats}, 
            encoder = HatsListEncoder,
        )
    else: 
        content = json.loads(request.body)

        #get Location object and put it in the content dict 
        try:
            location_href = content["location"]
            location = LocationVO.objects.get(import_href=location_href)
            content["location"] = location
        except LocationVO.DoesNotExist as e: 
            return JsonResponse(
                {"message":f"Invalid location id {e}"}, 
                status = 400
            )

        print("Creating Hat...")
        hat = Hats.objects.create(**content)
        return JsonResponse (
            hat, 
            encoder = HatsDetailEncoder, 
            safe=False
        )
    
@require_http_methods(["GET", "PUT", "DELETE"])    
def api_show_hat(request, pk): 
    if request.method == "GET": 
        hat = Hats.objects.get(id=pk)
        return JsonResponse(
            hat, 
            encoder = HatsDetailEncoder, 
            safe=False,
        )
    elif request.method == "DELETE": 
        count, _ = Hats.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else: 
        content = json.loads(request.body)
        
        Hats.objects.filter(id=pk).update(**content)
        hat = Hats.objects.get(id=pk)
        return JsonResponse (
            hat, 
            encoder = HatsDetailEncoder, 
            safe=False,
        )


def list_locations(request): 
    if request.method == "GET": 
        locations = LocationVO.objects.all() 
        return JsonResponse ( 
            locations, 
            encoder = LocationVODetailEncoder, 
            safe=False
        )
    
def show_location_detail(request, pk): 
    if request.method == "GET": 
        location = LocationVO.objects.get(id=pk)
        return JsonResponse ( 
            location, 
            encoder=LocationVODetailEncoder, 
            safe=False
        )