from django.urls import path
from .views import api_list_hats, api_show_hat, list_locations, show_location_detail

urlpatterns = [
     path("hats/", api_list_hats, name="api_list_hats"),
    path("hats/<int:pk>/", api_show_hat, name="api_show_hat"),
    path("location/", list_locations, name="api_list_locations"), 
    path("location/<int:pk>/", show_location_detail, name="api_show_location"), 
]