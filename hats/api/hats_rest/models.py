from django.db import models

# Create your models here.
class LocationVO(models.Model): 
    closet_name = models.CharField(max_length=100)
    section_number = models.PositiveSmallIntegerField()
    shelf_number = models.PositiveSmallIntegerField() 
    import_href = models.CharField(max_length=200, unique=True, null=True)

    def __str__(self):
        return f"Location (closet_name: {self.closet_name} section_number: {self.section_number} shelf_number: {self.shelf_number} import_href: {self.import_href})"

class Hats(models.Model): 
    fabric = models.CharField(max_length=100)
    style_name = models.CharField(max_length=100)
    color = models.CharField(max_length = 100)
    url = models.URLField(null=True)

    location = models.ForeignKey(
        LocationVO, 
        related_name = "hats", 
        on_delete=models.CASCADE, 
    )

# Makes model visible on admin 
    def __str__(self):
        return self.style_name