from django.shortcuts import render
from .models import Shoe, BinVO
from django.http import JsonResponse
import json
from common.json import ModelEncoder

# from .models import Shoe
from django.views.decorators.http import require_http_methods

# Create your views here.


class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "closet_name",
        "bin_number",
        "bin_size",
        "id",
    ]


class ShoesListEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "id",
        "model_name",
        "manufacturer",
        "color",
        "picture_url",
        "bin_id",
    ]

    encoders = {
        "bin_id": BinVOEncoder(),
    }


class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "id",
        "model_name",
        "bin_id",
        "manufacturer",
    ]
    encoders = {
        "bin_id": BinVOEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_list_shoes(request):

    if request.method == "GET":
        shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoesListEncoder,
        )
    else:
        content = json.loads(request.body)
        bin = BinVO.objects.get(href=content["bin_id"])
        content["bin_id"] = bin
        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "PUT", "DELETE"])
def api_shoe(request, pk):
    if request.method == "GET":
        try:
            shoe = Shoe.objects.get(id=pk)
            return JsonResponse(
                shoe,
                encoder=ShoeDetailEncoder,
                safe=False,
            )
        except Shoe.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            count, _ = Shoe.objects.filter(id=pk).delete()
            return JsonResponse({"deleted": count > 0})
        except Shoe.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else:  # PUT
        try:
            content = json.loads(request.body)
            shoe = Shoe.objects.get(id=pk)
            props = ["manufacturer", "model_name", "color", "bin_id"]
            for prop in props:
                if prop in content:
                    setattr(shoe, prop, content[prop])
            shoe.save()
            return JsonResponse(
                shoe,
                encoder=ShoeDetailEncoder,
                safe=False,
            )
        except Shoe.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
