from django.db import models
from django.core.exceptions import ObjectDoesNotExist
from django.urls import reverse


class BinVO(models.Model):
    closet_name = models.CharField(max_length=200, unique=True)
    bin_number = models.PositiveSmallIntegerField()
    bin_size = models.PositiveSmallIntegerField()
    href = models.CharField(max_length=200, null=True)

    def __str__(self):
        return self.closet_name

    def get_api_url(self):
        return reverse("api_bin", kwargs={"pk": self.pk})

    class Meta:
        ordering = ("closet_name", "bin_number", "bin_size")


# Create your models here.
class Shoe(models.Model):
    manufacturer = models.CharField(max_length=200)
    model_name = models.CharField(max_length=200)
    color = models.CharField(max_length=200)
    picture_url = models.URLField(null=True)
    bin_id = models.ForeignKey(
        BinVO,
        related_name="shoes",
        on_delete=models.PROTECT,
    )

    def __str__(self):
        return self.model_name

    def get_api_url(self):
        return reverse("api_shoe", kwargs={"pk": self.pk})
