# Generated by Django 4.0.3 on 2024-03-13 16:46

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('shoes_rest', '0003_rename_shoes_shoe'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='binvo',
            options={'ordering': ('closet_name', 'bin_number', 'bin_size')},
        ),
    ]
