# Generated by Django 4.0.3 on 2024-03-14 02:21

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('shoes_rest', '0004_alter_binvo_options'),
    ]

    operations = [
        migrations.AddField(
            model_name='binvo',
            name='href',
            field=models.CharField(max_length=200, null=True),
        ),
        migrations.AlterField(
            model_name='shoe',
            name='bin_id',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='shoes', to='shoes_rest.binvo'),
        ),
    ]
