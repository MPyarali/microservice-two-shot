from .keys import PEXELS_API_KEY
import requests


def get_photo(city, state):
    # create some query parameters
    params = {
        "query": f"{city}, {state}",
        "per_page": "1",
    }
    # construct the url
    url = "https://api.pexels.com/v1/search"
    # create some headers
    headers = {"Authorization": PEXELS_API_KEY}
    # send the request
    response = requests.get(url, headers=headers, params=params)
    try:
        # parse the json
        content = response.json()
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except (KeyError, IndexError, requests.exceptions.JSONDecoderError):
        return {"picture_url": None}
