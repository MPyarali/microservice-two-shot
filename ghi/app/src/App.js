import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import HatsList from './HatsList.js';
import HatsForm from './HatsForm.js';
import HatsHome from './HatsHome.js';
import ShoesList from './ShoesList.js';
import ShoeForm from './ShoeForm.js';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="hats">
              <Route path="" element={<HatsHome/>} />
              <Route path="new" element={<HatsForm />} />
          </Route>
          <Route path="shoes">
              <Route path="" element={<ShoesList/>} />
              <Route path="new" element={<ShoeForm />} />
          </Route>
        </Routes>

      </div>
    </BrowserRouter>
  );
}

export default App;
