import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';

function HatsColumn(props) {
    const handleDelete = async (hats) => {
         const url = 'http://localhost:8090/api/hats/' + hats; 
        const fetchConfig = {
            method: "delete", 
        }; 
    
        const response= await fetch(url, fetchConfig); 
    
        if (response.ok) {
        const data = await response.json(); 
        props.getHats();
        }
    }
  return (
    <div className="col">
      {props.list.map(data => {
        const hats = data;
        return (
          <div key={hats.id} className="card mb-3 shadow">
            <img
              src={hats.url}
              className="card-img-top"
              alt='Picture of Hat'
            />
            <div className="card-body">
              <h5 className="card-title mb-2 text-muted">Hat #{hats.id}</h5>
              <h4 className="card-subtitle">Color: {hats.color} - Fabric: {hats.fabric} - Style: {hats.style_name}</h4>
              <h5 className="card-title text-muted">Location: {hats.location.closet_name}/{hats.location.section_number}/{hats.location.shelf_number}</h5>
            </div>
            <div className="text-center"> 
                <button onClick={() => handleDelete(hats.id)} className="btn btn-danger mb-3">Delete</button>
            </div> 
          </div>
        );
      })}
    </div>
  );
}





function HatsHome(props) {
  const [hatsColumns, setHatsColumns] = useState([[], [], []]);

  async function getHats () {
    const url = 'http://localhost:8090/api/hats/';

    try {
      const response = await fetch(url);
      if (response.ok) {
        const data = await response.json();

        const requests = [];
        for (let hats of data.hats) {
          const detailUrl = `http://localhost:8090/api/hats/${hats.id}`;
          requests.push(fetch(detailUrl));
        }

        const responses = await Promise.all(requests);
        const hatsColumns = [[], [], []];

        let i = 0;
        for (const hatsResponse of responses) {
          if (hatsResponse.ok) {
            const details = await hatsResponse.json();
            hatsColumns[i].push(details);
            i = i + 1;
            if (i > 2) {
              i = 0;
            }
          } else {
            console.error(hatsResponse);
          }
        }
        setHatsColumns(hatsColumns);
      }
    } catch (e) {
      console.error(e);
    }
  }

  useEffect(() => {
    getHats();
  }, []);

  return (
    <>
      <div className="px-4 py-5 my-5 mt-0 text-center bg-info">
        <img className="bg-white rounded shadow d-block mx-auto mb-4" src="/logo.svg" alt="" width="600" />
        <h1 className="display-5 fw-bold">Wardrobify - Hats</h1>
        <div className="col-lg-6 mx-auto">
          <p className="lead mb-4">
            The only resource you'll ever need to organize all of your hats!
          </p>
          <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
            <Link to="/hats/new" className="btn btn-primary btn-lg px-4 gap-3">Create a new hat entry!</Link>
          </div>
        </div>
      </div>
      <div className="container">
        <h2>Hats in your Wardrobe</h2>
        <div className="row">
          {hatsColumns.map((hatsList, index) => {
            return (
              <HatsColumn key={index} list={hatsList} getHats={getHats} />
            );
          })}
        </div>
      </div>
    </>
  );
}


export default HatsHome;
