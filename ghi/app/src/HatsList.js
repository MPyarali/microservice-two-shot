import React, { useEffect, useState } from 'react';

function HatsList(props){
    const [hats, setHats] = useState([]);
    const fetchData = async () => {
        const url = "http://localhost:8090/api/hats/";
        const response = await fetch(url);
        if (response.ok){
            const data = await response.json();
            setHats(data.hats);
        }
    }
    useEffect(() => {
        fetchData();
    }, [])

   return (
    <table className="table table-striped">
    <thead>
      <tr>
        <th>Fabric</th>
        <th>Style Name</th>
        <th>Color</th>
        <th>URL Link for photo </th>
        <th>Location</th>
      </tr>
    </thead>
    <tbody>
      {hats.map(hats => {
        return (
          <tr key={hats.id}>
            <td>{ hats.fabric }</td>
            <td>{ hats.style_name }</td>
            <td>{ hats.color }</td>
            <td>{ hats.url }</td>
            <td>{ hats.id }</td>
          </tr>
        );
      })}
    </tbody>
  </table>
   );
}

export default HatsList; 