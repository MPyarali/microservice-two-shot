import React, { useEffect, useState } from 'react';

function ShoesList(){
    const [shoes, setShoes] = useState([]);
    const fetchData = async () => {
        const url = "http://localhost:8080/api/shoes/";
        const response = await fetch(url);
        if (response.ok){
            const data = await response.json();
            setShoes(data.shoes);
            // console.log(data.shoes)
        }


    }
    useEffect(() => {
        fetchData();
    }, [])

    const handleDelete = async (shoe) => {
        // shoe.preventDefault();
        alert("Are you sure you want to delete " + shoe.model_name + "?")
        const url = 'http://localhost:8080/api/shoes/' + shoe.id;
        const fetchConfig = {
            method: "delete",
        }
        const response = await fetch(url, fetchConfig);
        if (response.ok){
            const data = await response.json();
            // setShoes(shoes);
            console.log(shoe.id);
            fetchData();
        }
    }

    return (
        <>
            <div className="px-4 py-5 my-5 text-center">
                <h1 className="display-5 fw-bold">WARDROBIFY!</h1>
                <div className="col-lg-6 mx-auto">
                    <p className="lead mb-4">
                    Need to keep track of your shoes and hats? We have
                    the solution for you!
                    </p>
                </div>
            </div>
            <table className="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>Model name</th>
                        <th>Manufacturer</th>
                        <th>Color</th>
                        <th>id</th>
                        <th>Bin Number</th>
                        <th>Image</th>
                    </tr>
                </thead>
                <tbody>
                    {shoes.map(shoe => {
                        return (
                            <tr key={ shoe.href }>
                                <td>{ shoe.model_name }</td>
                                <td>{ shoe.manufacturer }</td>
                                <td>{ shoe.color }</td>
                                <td>{ shoe.id }</td>
                                <td>{ shoe.bin_id.bin_number}</td>
                                <td><img src={ shoe.picture_url } alt="test" length="100" width="100"/></td>
                                <td><button className="btn btn-danger" onClick={() => handleDelete(shoe)}>Delete</button></td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </>
    );
}

export default ShoesList;
