# Wardrobify

Team:

* Brian Weber - Shoes
* Monika Pyarali - Hats 

## Design

## Shoes microservice

The Shoes microservice is a RESTful application service that allows a user to create a shoe entry, to show a list of all shoe entries along with its details, and delete a specific shoe from the user's list.  

The Shoe model contain properties that allow a user to store a shoe model name, a manufacturer, the shoe's color, a URL to an image of the shoe as well as the Bin ID.

The wardrobe API is a way to organize each Shoe item into an organizational Bin system.  Each shoe will be located in a Bin.  Each bin has its own closet name and closet number for reference.  

The Shoe microservice is integrated with the wardrobe API in that each shoe is located in a Bin that is stored in the wardrobe API.  That Bin is assigned as a foreign key to the Shoe model.  The Bin ID can be reflected in the details of a shoe in order to find its location in the wardrobe. 

## Hats microservice

Hats microservice creates a Hats model that is linked to the Location model as a foreign object. This model is used to GET, POST, PUT, and DELETE using the RESTful API structure. The hats microservice front-end uses React to display a homepage of hats available, along with details of the fabric, color, and style of hat and the location. Wardrobify also includes a form to create a new hat by passing in these details and provides users with the functionality of deleting a hat. 

